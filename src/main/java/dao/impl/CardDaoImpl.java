package dao.impl;

import dao.CardDao;
import model.Card;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class CardDaoImpl implements CardDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
//        this.jdbcTemplateObject.execute("CREATE TABLE card(" +
//                "id serial primary key, name VARCHAR(255), type VARCHAR(255), cost integer, offense integer, defense integer, " +
//                "abilities VARCHAR(255), flavorText VARCHAR(255), rating real, userId VARCHAR(50) REFERENCES users(username))");
    }

    public void create(String name, String type, int cost, int offense, int defense, String abilities, String flavorText, String userId) {
        String SQL = "insert into Card (name, type, cost, offense, defense, abilities, flavorText, rating, userId) values " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?)";

        jdbcTemplateObject.update( SQL, name, type, cost, offense, defense, abilities, flavorText, 0.0f, userId);
        System.out.println("Created Record Name = " + name);
        return;
    }

    public Card getCard(Integer id) {
        String SQL = "select * from Card where id = ?";
        Card card = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new CardMapper());
        return card;
    }

    public List<Card> listCards() {
        String SQL = "select * from Card";
        List <Card> cards = jdbcTemplateObject. query(SQL,
                new CardMapper());
        return cards;
    }

    public void delete(Integer id){
        String SQL = "delete from Card where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }

}