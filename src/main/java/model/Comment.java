package model;

/**
 * Created by jacques on 03.05.16.
 */
public class Comment {
    private long id;
    private String commentText;
    private String userId;
    private long cardId;

    public Comment(long id, String commentText, String userId, long cardId) {
        this.id = id;
        this.commentText = commentText;
        this.userId = userId;
        this.cardId = cardId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        userId = userId;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        cardId = cardId;
    }
}
