package dao.impl;

import dao.UserDao;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
//        this.jdbcTemplateObject.execute("CREATE TABLE users(" +
//                "username VARCHAR(255) primary key, password VARCHAR(255), enabled boolean)");
    }

    public void create(String username, String password, boolean enabled) {
        String SQL = "insert into Users (username, password, enabled) values (?, ?, ?)";

//        jdbcTemplateObject.update( SQL, new Object[]{username, password, enabled}, new int[]{ Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN });
        jdbcTemplateObject.update( SQL, username, password, enabled);
        System.out.println("Created Record Name = " + username);
        return;
    }

    public User getUser(String username) {
        String SQL = "select * from Users where username = ?";
        User user;
        try {
            user = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{username}, new UserMapper());
        } catch(Exception e) {
            user = null;
        }
        return user;
    }

    public List<User> listUsers() {
        String SQL = "select * from Users";
        List <User> users = jdbcTemplateObject.query(SQL,
                new UserMapper());
        return users;
    }

    public void delete(String username){
        String SQL = "delete from Users where username = ?";
        jdbcTemplateObject.update(SQL, username);
        System.out.println("Deleted Record with ID = " + username );
        return;
    }

}