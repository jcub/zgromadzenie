package dao.impl;

import dao.CommentDao;
import model.Comment;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class CommentDaoImpl implements CommentDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
//        this.jdbcTemplateObject.execute("CREATE TABLE comment(" +
//                "id serial, commentText VARCHAR(255), userId VARCHAR(50) REFERENCES users(username), cardId serial REFERENCES card(id))");
    }

    public void create(String commentText, String userId, long cardId) {
        String SQL = "insert into Comment (commentText, userId, cardId) values " + "(?, ?, ?)";

        jdbcTemplateObject.update( SQL, commentText, userId, cardId);
        System.out.println("Created Record Name = " + commentText);
        return;
    }

    public Comment getComment(Integer id) {
        String SQL = "select * from Comment where id = ?";
        Comment comments = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new CommentMapper());
        return comments;
    }

    public List<Comment> getCommentwithCardID(Integer cardId) {
        String SQL = "select * from Comment where cardid = ?";
        List <Comment> comments = jdbcTemplateObject.query(SQL,
                new Object[]{cardId}, new CommentMapper());
        return comments;
    }

    public List<Comment> listComments() {
        String SQL = "select * from Comment";
        List <Comment> comments = jdbcTemplateObject.query(SQL,
                new CommentMapper());
        return comments;
    }

    public void delete(Integer id){
        String SQL = "delete from Comment where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }

}