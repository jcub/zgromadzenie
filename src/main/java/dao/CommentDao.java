package dao;

import model.Comment;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by root on 5/5/16.
 */
public interface CommentDao {
    /**
     * This is the method to be used to initialize
     * database resources ie. connection.
     */
    public void setDataSource(DataSource ds);
    /**
     * This is the method to be used to create
     * a record in the Student table.
     */
    public void create(String commentText, String userId, long cardId);
    /**
     * This is the method to be used to list down
     * a record from the Student table corresponding
     * to a passed student id.
     */
    public Comment getComment(Integer id);

    public List<Comment> getCommentwithCardID(Integer cardId);
    /**
     * This is the method to be used to list down
     * all the records from the Student table.
     */
    public List<Comment> listComments();
    /**
     * This is the method to be used to delete
     * a record from the Student table corresponding
     * to a passed student id.
     */
    public void delete(Integer id);
}
