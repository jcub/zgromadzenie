package dao.impl;

import model.Comment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentMapper implements RowMapper<Comment> {
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Comment comment = new Comment(rs.getInt("id"), rs.getString("commentText"), rs.getString("userId"), rs.getLong("cardId"));
        return comment;
    }
}