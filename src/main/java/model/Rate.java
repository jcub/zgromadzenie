package model;

/**
 * Created by jacques on 03.05.16.
 */
public class Rate {
    private long id;
    private String userId;
    private long CardId;
    private int rate;

    public Rate(long id, String userId, long cardId, int rate) {
        this.id = id;
        this.userId = userId;
        CardId = cardId;
        this.rate = rate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getCardId() {
        return CardId;
    }

    public void setCardId(long cardId) {
        CardId = cardId;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
