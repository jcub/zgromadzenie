package dao.impl;

import model.Card;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CardMapper implements RowMapper<Card> {
    public Card mapRow(ResultSet rs, int rowNum) throws SQLException {
        Card card = new Card(rs.getInt("id"), rs.getString("name"), rs.getString("type"), rs.getInt("cost"),
                rs.getInt("offense"), rs.getInt("defense"), rs.getString("abilities"), rs.getString("flavortext"),
                rs.getFloat("rating"), rs.getString("userid"));
        return card;
    }
}