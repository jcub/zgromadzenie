package controllers;

import dao.impl.CardDaoImpl;
import dao.impl.CommentDaoImpl;
import dao.impl.RateDaoImpl;
import dao.impl.UserDaoImpl;
import model.Card;
import model.Comment;
import model.Rate;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.sql.DataSource;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private DataSource dataSource;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String home(@RequestParam(value="name", required=false, defaultValue="guest") String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public String home2(@RequestParam(value="name", required=false, defaultValue="guest") String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/register")
    public String register(Model model) {
        return "register";
    }

    @RequestMapping(value="/register", method= RequestMethod.POST)
    public String registerSubmit(@RequestParam("username") String username, @RequestParam("password") String password,
                                 @RequestParam("password2") String password2, Model model) {
        UserDaoImpl userDao = new UserDaoImpl();
        userDao.setDataSource(dataSource);
        if (password.equals(password2)) {
            User user = userDao.getUser(username);
            if (user == null) {
                userDao.create(username, password, true);
                model.addAttribute("success", "User successfully created");
            }
            else {
                model.addAttribute("error", "User with that username already exists");
            }
        }
        else {
            model.addAttribute("error", "Passwords have to be the same");
        }
        return "register";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/cards/list")
    public String cardList(Model model) {
        CardDaoImpl card = new CardDaoImpl();
        card.setDataSource(dataSource);
        List<Card> cards = card.listCards();
        model.addAttribute("cards", cards);
        return "cardList";
    }

    @RequestMapping("/cards/details/{cardid}")
        public String cardDetails(@PathVariable(value="cardid") int cardId, Model model){
        CardDaoImpl cardDao = new CardDaoImpl();
        RateDaoImpl rateDao = new RateDaoImpl();
        CommentDaoImpl commentDao = new CommentDaoImpl();
        cardDao.setDataSource(dataSource);
        commentDao.setDataSource(dataSource);
        rateDao.setDataSource(dataSource);
        Card card = cardDao.getCard(cardId);
        List<Comment> comments = commentDao.getCommentwithCardID(cardId);
        List<Rate> rates = rateDao.getRatewithCardID(cardId);
        float averageRate = 0.0f;
        for (Rate r: rates) {
            averageRate += r.getRate();
        }

        model.addAttribute("card", card);
        model.addAttribute("comments", comments);
        model.addAttribute("rates", rates);
        model.addAttribute("averageRate", averageRate / rates.size());
        return "cardDetails";
        }


    @RequestMapping("/comments/add/{cardId}")
    public String commentAdd(@PathVariable(value="cardId") int cardId, Model model) {
        CardDaoImpl cardDao = new CardDaoImpl();
        cardDao.setDataSource(dataSource);
        Card card = cardDao.getCard(cardId);
        model.addAttribute("card", card);
        return "addComment";
    }

    @RequestMapping("/rates/add/{cardId}")
    public String rateAdd(@PathVariable(value="cardId") int cardId, Model model) {
        CardDaoImpl cardDao = new CardDaoImpl();
        cardDao.setDataSource(dataSource);
        Card card = cardDao.getCard(cardId);
        model.addAttribute("card", card);
        return "addRate";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/cards/add")
    public String cardAdd() {
        return "addCard";
    }

    @RequestMapping(value="/cards/add", method= RequestMethod.POST)
    public String cardAddSubmit(@RequestParam("name") String name, @RequestParam("type") String type,
                                 @RequestParam("cost") String cost, @RequestParam("offense") String offense,
                                @RequestParam("defense") String defense, @RequestParam("abilities") String abilities,
                                @RequestParam("flavorText") String flavorText, Model model) {
        CardDaoImpl cardDao = new CardDaoImpl();
        cardDao.setDataSource(dataSource);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            cardDao.create(name, type, Integer.parseInt(cost), Integer.parseInt(offense), Integer.parseInt(defense),
                    abilities, flavorText, "test");
            model.addAttribute("success", "Card successfully created");
        } catch(Exception e) {
            model.addAttribute("error", "Wrong inputs");
        }
        return "addCard";
    }

    @RequestMapping(value="/comments/add/{cardId}", method= RequestMethod.POST)
    public String commentAddSubmit(@PathVariable(value="cardId") int cardId, @RequestParam("commentText") String commentText, Model model) {
        CommentDaoImpl commentDao = new CommentDaoImpl ();
        commentDao.setDataSource(dataSource);
        CardDaoImpl cardDao = new CardDaoImpl();
        cardDao.setDataSource(dataSource);
        Card card = cardDao.getCard(cardId);
        model.addAttribute("card", card);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            commentDao.create(commentText, "test", cardId);
            model.addAttribute("success", "Comment successfully created");
        } catch(Exception e) {
            model.addAttribute("error", "Wrong inputs");
        }
        return "addComment";
    }


    @RequestMapping(value="/rates/add/{cardId}", method= RequestMethod.POST)
    public String rateAddSubmit(@PathVariable(value="cardId") int cardId, @RequestParam("rate") String rate, Model model) {
        RateDaoImpl rateDao = new RateDaoImpl ();
        rateDao.setDataSource(dataSource);
        CardDaoImpl cardDao = new CardDaoImpl();
        cardDao.setDataSource(dataSource);
        Card card = cardDao.getCard(cardId);
        model.addAttribute("card", card);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            int avRate = Integer.parseInt(rate);
            if (avRate < 0 || avRate > 5){
                throw new Exception();
            }
            rateDao.create("test", cardId, avRate);
            model.addAttribute("success", "Card successfully rated");
        } catch(Exception e) {
            model.addAttribute("error", "Wrong inputs");
        }
        return "addRate";
    }



}