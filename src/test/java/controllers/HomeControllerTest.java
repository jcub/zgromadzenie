package controllers;

import app.Application;
import model.User;
import model.Card;
import model.Comment;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class HomeControllerTest {
    private MockMvc mvc;

    private User user;
    private Card card;
    private Comment comment;

    @Before
    public void setUp() throws Exception {
    	this.user = new User("test", "test", true);
    	this.card = new Card(1, "test", "monster", 3, 4, 6, "testowe", "z testu", 0.0f, this.user.getUsername());
    	this.comment = new Comment(1, "test", this.user.getUsername(), this.card.getId());

    	InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");

        mvc = MockMvcBuilders.standaloneSetup(new HomeController())
            .setViewResolvers(viewResolver)
            .build();
    }

    @Test
    public void home() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void home2() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/home").accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk());
    }

    @Test
    public void login() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/login").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void register() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/register").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

}