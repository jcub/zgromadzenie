package model;

/**
 * Created by jacques on 30.04.16.
 */
public class Card {
    public long id;
    public String name;
    public String type;
    public int cost;
    public int offense = 0;
    public int defense = 0;
    public String abilities = null;
    public String flavorText = null;
    public float rating = 0.0f;
    public String userId;

    public Card(long id, String name, String type, int cost, int offense, int defense, String abilities, String flavorText, float rating, String userId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.offense = offense;
        this.defense = defense;
        this.abilities = abilities;
        this.flavorText = flavorText;
        this.rating = rating;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {return userId;    }

    public void setUserId(String userId) {this.userId = userId;    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getOffense() {
        return offense;
    }

    public void setOffense(int offense) {
        this.offense = offense;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public String getAbilities() {
        return abilities;
    }

    public void setAbilities(String abilities) {
        this.abilities = abilities;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public void setFlavorText(String flavorText) {
        this.flavorText = flavorText;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }





}
