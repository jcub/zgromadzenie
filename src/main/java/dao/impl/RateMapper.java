package dao.impl;

import model.Rate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RateMapper implements RowMapper<Rate> {
    public Rate mapRow(ResultSet rs, int rowNum) throws SQLException {
        Rate rate = new Rate(rs.getInt("id"), rs.getString("userId"), rs.getLong("cardId"), rs.getInt("rate"));
        return rate;
    }
}