package dao.impl;

import dao.RateDao;
import model.Rate;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class RateDaoImpl implements RateDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
//        this.jdbcTemplateObject.execute("CREATE TABLE rate(" +
//                "id serial primary key, userId VARCHAR(50) REFERENCES users(username), cardId serial REFERENCES card(id), rate integer)");
    }

    public void create(String userId, long cardId, int rate) {
        String SQL = "insert into Rate (userId, cardId, rate) values " +
                "(?, ?, ?)";

        jdbcTemplateObject.update( SQL, userId, cardId, rate);
        System.out.println("Created Record Name = " + rate);
        return;
    }

    public Rate getRate(Integer id) {
        String SQL = "select * from Rate where id = ?";
        Rate rate = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new RateMapper());
        return rate;
    }

    public List<Rate> getRatewithCardID(Integer cardId) {
        String SQL = "select * from Rate where cardid = ?";
        List <Rate> rates = jdbcTemplateObject.query(SQL,
                new Object[]{cardId}, new RateMapper());
        return rates;
    }

    public List<Rate> listRates() {
        String SQL = "select * from Rate";
        List <Rate> rates = jdbcTemplateObject.query(SQL,
                new RateMapper());
        return rates;
    }

    public void delete(Integer id){
        String SQL = "delete from Rate where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }

}