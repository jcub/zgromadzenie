package model;

public class User {
    private String username;
    private String password;
    private boolean enabled;

    public User(String username, String password, boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getRole() {
        return enabled;
    }

    public void setRole(boolean role) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return String.format(
                "User[name='%s', password='%s']",
                username, password);
    }


}