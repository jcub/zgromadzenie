<?xml version="1.0" encoding="UTF-8"?>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
      xmlns:tiles="http://tiles.apache.org/tags-tiles" xmlns:jsp="http://www.springframework.org/schema/aop">
<head>
    <title>Card Creator</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>
</head>
<body>
<!-- menu -->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Card Creator</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/home">Home</a></li>
                <li><a href="/cards/list">Cards</a></li>
                <li><a href="/cards/add">Add card</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Log in</a></li>
                <li><a href="/register">Register</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div>
    <ul>
        <li>Name: <p th:text="${card.name}"></p></li>
        <li>Type: <p th:text="${card.type}"></p></li>
        <li>Cost: <p th:text="${card.cost}"></p></li>
        <li>Offense: <p th:text="${card.offense}"></p></li>
        <li>Defense: <p th:text="${card.defense}"></p></li>
        <li>Abilities: <p th:text="${card.abilities}"></p></li>
        <li>Flavor Text: <p th:text="${card.flavorText}"></p></li>
        <li>Rating: <p th:text="${averageRate}"></p></li>
        <li>User: <p th:text="${card.userId}"></p></li>
    </ul>
    <br />
    <p><a href="details.html" th:href="@{/comments/add/{cardid}(cardid=${card.id})}">Add comment</a> </p>
    <p><a href="details.html" th:href="@{/rates/add/{cardid}(cardid=${card.id})}">Rate this card</a> </p>
    <br />
    Comments:
    <ul class="list-group" th:each="comment : ${comments}">
        <li class="list-group-item">
            <h4 class="media-heading" th:text="${comment.userId}">Media heading</h4>
            <p class="media" th:text="${comment.commentText}"></p>
        </li>
    </ul>
    <!--
    <ul th:each="rate : ${rates}">
        <li th:text="${rate.rate}"></li>
    </ul>
    -->

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
</body>
</html>