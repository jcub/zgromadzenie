package dao;

import model.Rate;

import javax.sql.DataSource;
import java.util.List;

public interface RateDao {
    /**
     * This is the method to be used to initialize
     * database resources ie. connection.
     */
    public void setDataSource(DataSource ds);
    /**
     * This is the method to be used to create
     * a record in the Student table.
     */
    public void create(String userId, long cardId, int rate);
    /**
     * This is the method to be used to list down
     * a record from the Student table corresponding
     * to a passed student id.
     */
    public Rate getRate(Integer id);

    public List<Rate> getRatewithCardID(Integer cardId);
    /**
     * This is the method to be used to list down
     * all the records from the Student table.
     */
    public List<Rate> listRates();
    /**
     * This is the method to be used to delete
     * a record from the Student table corresponding
     * to a passed student id.
     */
    public void delete(Integer id);
}